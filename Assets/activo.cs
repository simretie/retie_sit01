﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSAvancedUI;
using UnityEngine.UI;
using NSInterfaz;

public class activo : MonoBehaviour {


    public panelZonas[] panlesConTempletesVaibles;

    // Use this for initialization
    void Start () {
        activarselecciontemplete();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void activodeVerdad()
    {
        if (gameObject.activeSelf)
        {
            Debug.Log("este punto esta activo");
        }
        else
        {
            Debug.Log("este punto esta desactivado");
        }
    }

    public void activarselecciontemplete()
    {
        for (int i = 0; i < panlesConTempletesVaibles.Length; i++)
            panlesConTempletesVaibles[i].decidirtemplete();
    }

}
