﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalificarRetie3 : MonoBehaviour {

	// Use this for initialization
	// zona 1
	public SelectorRetie3[] CajasZona1;
	public InputField Long_SPT; // longitud de varilla de puesta a tierra
	public InputField NumCertificadoText;
	public int PosIncorrectoCaja3 = 3;
	public int Num_Certificado = 0;
	public int[] EstructurasCorrectas; // index de postes correctos
	public int Cajas6Z0Correcto = 0;

	// zona 2
	public SelectorRetie3[] CajasZona2;
	public int Cajas6Z1Correcto = 0;
	public int CableEncauchetado = 2;
	public int AisladorTCorrecto = 1;
	public int AisladorReteCaja9 = 1;
	public int Descargadores = 0;

	//zona 3
	public SelectorRetie3[] CajasZona3;
	public int MaterialTemplete = 1;
	public int[] EstructuraCorrectaz3;

	public int VecesCertificado = 0;
	public float calibreSPTConductor = 2.4f;
	public InputField[] Calibre_SPT;

	public float Calificacion = 0f;
	public bool camposCorrectos = true;
	public bool EmptyCampos = false;
	public float NotaTotal = 0f;

	[Header("Settings para reiniciar")]
	public GameObject[] ZonasPrincipales;
	public GameObject ZonsPBotones = null;
	public GameObject BotonesYNumeros = null;
	public GameObject[] ZonasAOcultar;
	public GameObject[] ZonasActivas;

	[Header("Valores para la formula")]
	public TEXDraw[] formulas; 
	private float[] valores = new float[3];
	private float resultadoFormula = 0f;

	void Start () {

		GenerarFormula ();

	}
	
	// Update is called once per frame
	void Update () {
		
	}public void Verificar(){

		camposCorrectos = true;
		EmptyCampos = false;
		Calificacion = 0;

		////zona 0
		if(CajasZona1[0].ActualSeleccionado == CajasZona1[1].ActualSeleccionado && CajasZona1[1].ActualSeleccionado == CajasZona1[2].ActualSeleccionado
			&& CajasZona1[0].ActualSeleccionado == CajasZona1[2].ActualSeleccionado  && (CajasZona2[5].ActualSeleccionado==1 || CajasZona2[5].ActualSeleccionado==2)){

			// correcto
			Calificacion+=0.04f;

		}else{

			// incorrecto

		}
			
		if( Long_SPT.text != null && Long_SPT.text == "2.44"){

			// correcto
			Calificacion+=0.04f;
 			Long_SPT.gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;

		}else{

			// incorrecto
			Long_SPT.gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = true;
			camposCorrectos = false;

			if(Long_SPT.text == ""){

				EmptyCampos = true;

			}

		}

		string num1 = NumCertificadoText.text;
		if(num1.Length <= 0){

			num1 = "1";
			EmptyCampos = true;

		}

		if(int.Parse( num1)  == Num_Certificado){

			// correcto
			NumCertificadoText.gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
			Calificacion+=0.04f;
 
		}else{

			// incorrecto
			NumCertificadoText.gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = true;
			camposCorrectos = false;


		}

		if(CajasZona1[3].ActualSeleccionado == EstructurasCorrectas[0] || CajasZona1[3].ActualSeleccionado == EstructurasCorrectas[1]){

			// correcto
			Calificacion+=0.04f;

		}else{

			//incorrecto


		}

		if(CajasZona1[4].ActualSeleccionado == Cajas6Z0Correcto){
			// correcto
			Calificacion+=0.04f;

		}else{
			// incorrecto

		}
		//// zona1

		if(CajasZona2[5].ActualSeleccionado == CajasZona1[6].ActualSeleccionado && CajasZona1[6].ActualSeleccionado == CajasZona1[7].ActualSeleccionado
			&& CajasZona1[5].ActualSeleccionado == CajasZona1[7].ActualSeleccionado && (CajasZona2[5].ActualSeleccionado==1 || CajasZona2[5].ActualSeleccionado==2)){

			// correcto
			Calificacion+=0.04f;

		}else{

			// incorrecto

		}
		if(CajasZona2[0].ActualSeleccionado == Cajas6Z1Correcto){

			// correcto
			Calificacion+=0.04f;

		}else{
			//incorrecto


		}if(CajasZona2[1].ActualSeleccionado == CableEncauchetado){

			// correcto
			Calificacion+=0.04f;

		}else{
			//incorrecto


		}if(CajasZona2[2].ActualSeleccionado == AisladorTCorrecto){

			// correcto
			Calificacion+=0.04f;

		}else{
			//incorrecto


		}if(CajasZona2[3].ActualSeleccionado == AisladorReteCaja9){

			// correcto
			Calificacion+=0.04f;

		}else{
			//incorrecto


		}if(CajasZona2[4].ActualSeleccionado == Descargadores){

			// correcto
			Calificacion+=0.04f;

		}else{
			//incorrecto


		}


		// zona 3
		if(CajasZona3[0].ActualSeleccionado == MaterialTemplete){

			// correcto
			Calificacion+=0.05f;

		}else{
			//incorrecto


		}if(CajasZona3[1].ActualSeleccionado == EstructuraCorrectaz3[0] && CajasZona3[1].ActualSeleccionado == EstructuraCorrectaz3[1]){

			// correcto
			Calificacion+=0.05f;

		}else{

			// incorrecto

		}


		num1 = Calibre_SPT[0].text;

		if(num1.Length <= 0){

			num1 = "2000";
			EmptyCampos = true;

		}
		if(float.Parse(num1.ToString()) <= (resultadoFormula+(resultadoFormula*0.05f)) && float.Parse(num1.ToString()) >= (resultadoFormula-(resultadoFormula*0.05f))){//mcm

			// correcto
			Calibre_SPT[0].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
			Calificacion+=0.05f;
 
		}else{

			// incorrecto
			Calibre_SPT[0].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = true;
			camposCorrectos = false;
			if(Calibre_SPT[0].text == ""){

				EmptyCampos = true;

			}

		}if(Calibre_SPT[1].text == "2/0"){//mcm

			// correcto
			Calibre_SPT[1].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
			Calificacion+=0.05f;
 
		}else{

			// incorrecto
			Calibre_SPT[1].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = true;
			camposCorrectos = false;

			if(Calibre_SPT[1].text == ""){

				EmptyCampos = true;

			}

		}

	}public void SetNumCertificado(int num){

		VecesCertificado += 1;
		Num_Certificado = num;

	}public float VerificarDatosUsuario(){

		Verificar();
		return Calificacion;

	}public void Reiniciar(){

		GenerarFormula();
		SelectorRetie3[] selectores = Resources.FindObjectsOfTypeAll<SelectorRetie3>();

		if(ZonsPBotones){
			ZonsPBotones.SetActive(true);
			BotonesYNumeros.SetActive(true);
		}
		for(var i = 0; i< ZonasPrincipales.Length; i++){

			ZonasPrincipales[i].SetActive(true);

		}


		foreach(SelectorRetie3 selector in selectores){

			selector.gameObject.SetActive(true);
			selector.RandomStart();
			selector.GetComponent<panelZonas>().Mostrar(false);
			selector.gameObject.SetActive(false);
		}

		for(var i = 0; i< ZonasPrincipales.Length; i++){

			ZonasPrincipales[i].SetActive(false);

		}for(var i = 0; i< ZonasAOcultar.Length; i++){

			ZonasAOcultar[i].SetActive(false);

		}for(var i = 0; i< ZonasActivas.Length; i++){

			ZonasActivas[i].SetActive(true);

		}


		NumCertificadoText.gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
		Calibre_SPT[1].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
		Calibre_SPT[0].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
		Long_SPT.gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;

		Long_SPT.text = "";
		NumCertificadoText.text = "";
		Calibre_SPT[0].text = "";
		Calibre_SPT[1].text = "";

	}public void GenerarFormula(){


		valores[0] = Random.Range(3f,6f);
		valores[1] = 7.06f;
		valores[2] = (Random.Range(3f,7f)/10f);

		formulas[0].text = valores[0].ToString("F2");
		formulas[1].text = valores[1].ToString();
		formulas[2].text = valores[2].ToString("F2");

		resultadoFormula = (valores[0]*valores[1])*Mathf.Sqrt(valores[2]);
		resultadoFormula = float.Parse(resultadoFormula.ToString("F2"));

	}
}
