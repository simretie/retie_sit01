﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSBoxMessage;
using NSTraduccionIdiomas;
using UnityEngine.SceneManagement;
using NSInterfaz;
using NSInterfazAvanzada;

public class ManagerRetieZonas2 : MonoBehaviour {

	// Use this for initialization
	private int zonaActual = -1;

	public GameObject[] InicialBotones;
	public panelZonas[] zonas;
	public panelZonas[] Telurometros;

	// en la zona 2 hay 3 fondos y para la captura hay que activar alguno de los 3
	public GameObject[] Zona1Retie2;
	private int IndexZona1Evaluada = 0;

	public GameObject[] ZonasActivas;
	public GameObject[] ZonasInternas;
	public GameObject BotonesyNumeros;

	private ManagerScene MgScenes;
	private GameObject canvasMeniIni;
	private GameObject camaraMenuInicio;

	public PanelInterfazEvaluacion PanelPreguntas;
	public PanelRegistroDeDatos PanelRegistro;
	public ClsTomarFotoRegistoyFondo controladorDedatos;
	public PanelFlexometro FlexometroRetie4;

	// telurometros de la zona 1
	public Telurometro[] teluZona1;
	public GameObject ZonasInternasZ1;
 
	void Start () {

		RandomStartSPT ();

		MgScenes = GameObject.FindGameObjectWithTag("ManagerScenes").GetComponent<ManagerScene>();
		camaraMenuInicio = GameObject.FindGameObjectWithTag("camaraMenuInicio");

		canvasMeniIni = GameObject.FindGameObjectWithTag("canvasMenuIni");
		canvasMeniIni.SetActive(false);
		camaraMenuInicio.SetActive(false);

	}
	
	// Update is called once per frame
	void Update () {
		
	}public void EntraAZona(int pos){

		zonas[pos].Mostrar(true);
		zonaActual = pos;

	}public void SalirDeZona(){ // zonas internas
		
		if(zonaActual != -1)
			zonas[zonaActual].Mostrar(false);
		else
			//BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeSalirSituacion", "¿Esta seguro que desea salir de la práctica?"), DiccionarioIdiomas._instance.Traducir("TextBotonAceptar", "ACEPTAR"), DiccionarioIdiomas._instance.Traducir("TextBotonCancelar", "CANCELAR"), salirDeLaPractica);
		
		zonaActual = -1;

	}public void salirDeZona()
	{
		if (zonaActual != -1)
		{
			zonaActual = -1;
		}
		BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeSalirSituacion", "¿Está seguro que desea abandonar la práctica?"), DiccionarioIdiomas._instance.Traducir("TextBotonAceptar", "ACEPTAR"), DiccionarioIdiomas._instance.Traducir("TextBotonCancelar", "CANCELAR"), salirDeLaPractica);

	}public void salirDeLaPractica()
	{
		//SceneManager.LoadScene("Login");
		MgScenes.descargarpractica();
	
	}public void RandomStartSPT(){


		Telurometro[] telurometros = Resources.FindObjectsOfTypeAll<Telurometro>();

		foreach(Telurometro tel in telurometros){

			tel.RandomSPT();

		}


	} public void PasarAgenerarReporte()
	{
		PanelRegistro.Mostrar(false);

		//BoxMessageManager._instance.MtdCreateBoxMessageDecision("¿Esta seguro que desea generar el reporte?","ACEPTAR","CANCELAR", activarPreguntas);
		activarPreguntas();

	}public void PasarAgenerarReporteRT3()
	{
		PanelRegistro.Mostrar(false);

		//BoxMessageManager._instance.MtdCreateBoxMessageDecision("¿Esta seguro que desea generar el reporte?","ACEPTAR","CANCELAR", activarPreguntasRT3);
		activarPreguntasRT3();

	}public void PasarAgenerarReporteRT4()
	{
		PanelRegistro.Mostrar(false);

		//BoxMessageManager._instance.MtdCreateBoxMessageDecision("¿Esta seguro que desea generar el reporte?","ACEPTAR","CANCELAR", activarPreguntasRT4);
		activarPreguntasRT4();

	}private void activarPreguntasRT3()
	{

		GameObject.FindObjectOfType<imagenZonasPdf>().InsertarZonasPdf();
		if (zonaActual != -1)
		{
			zonaActual = -1;
		}

		controladorDedatos.capturarRegistroyFOndo();
		PanelPreguntas.Mostrar(true);

		// reactivar zonas activas
		for(var i = 0; i<ZonasInternas.Length; i++){

			ZonasInternas[i].SetActive(true);

		} 



	}private void activarPreguntasRT4()
	{


		if (zonaActual != -1)
		{
			zonaActual = -1;
		}

		FlexometroRetie4.Mostrar(false);
		// ocultar zonas activas que se ve en el df
		for(var i = 0; i<ZonasInternas.Length; i++){

 			ZonasInternas[i].SetActive(false);

		} for(var i = 0; i<ZonasActivas.Length; i++){

 
 			ZonasActivas[i].SetActive(true);
 
		} 
		GameObject.FindObjectOfType<imagenZonasPdf>().InsertarZonasPdf();
		controladorDedatos.capturarRegistroyFOndo();
		PanelPreguntas.Mostrar(true);


	} private void activarPreguntas()
	{

		if(Telurometros.Length>0){
			for(var i = 0; i<Telurometros.Length; i++){

				Telurometros[i].gameObject.SetActive(true);
				if(i == 3)
					Telurometros[i].GetComponent<panelZonas>().Mostrar(true);

			}
		}
		Zona1Retie2[IndexZona1Evaluada].gameObject.SetActive(true);
		if(ZonasInternasZ1)
			ZonasInternasZ1.SetActive(true);
		teluZona1[IndexZona1Evaluada].gameObject.SetActive(true);

		GameObject.FindObjectOfType<imagenZonasPdf>().InsertarZonasPdf();
		if (zonaActual != -1)
		{
			zonaActual = -1;
		}
		controladorDedatos.capturarRegistroyFOndo();
		PanelPreguntas.Mostrar(true);

		// reactivar zonas activas
		for(var i = 0; i<ZonasActivas.Length; i++){

			ZonasActivas[i].SetActive(true);

		} 

	}public void ReinicirZonas(){
 
		zonaActual = -1;
		Telurometro[] tempTelu = Resources.FindObjectsOfTypeAll<Telurometro>();
		for(var i = 0;i < zonas.Length; i++){

			Telurometros[i].Mostrar(false);
			zonas[i].Mostrar(false);

		}for(var i = 0;i < tempTelu.Length; i++){

			tempTelu[i].ReiniciarPinzas();
			tempTelu[i].RandomSPT();
			tempTelu[i].RestaurarMedidasPorPica();
			tempTelu[i].VerificarCofigPinzas();
			tempTelu[i].ActivarTelurometro(false);

		}for(var i = 0; i<ZonasActivas.Length; i++){
			
			ZonasActivas[i].SetActive(true);

		}
			
		GameObject.FindObjectOfType<ControladorDeDatos>().IniciarNuevaSesionSituacion();
		GameObject.FindObjectOfType<CalificarRetie2>().reinicarCampos();
		GameObject.FindObjectOfType<Graficadora>().RandonHeightZona1();
		BotonesyNumeros.SetActive(true);

		for(var i = 0; i<InicialBotones.Length; i++){

			InicialBotones[i].SetActive(false);

		}

		for(var i =0; i<teluZona1.Length; i++){

			teluZona1[i].ReiniciarPinzas();
			teluZona1[i].RandomSPT();

		}
		ZonasInternas[0].SetActive(false);
		ZonasInternas[1].SetActive(false);
		ZonasInternas[2].SetActive(false);

		if(ZonasInternasZ1)
			ZonasInternasZ1.SetActive(false);

	}public void SetIndexZona1Evaluada(int pos){

		IndexZona1Evaluada = pos;

	}
}
