﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSEvaluacion
{
    public class ControladorDatosSesion : MonoBehaviour
    {

        #region members

        private byte cantidadIntentos;

        private float tiempoSituacion;

        private bool contarTiempoSituacion;
        [SerializeField]
        private Evaluacion clsEvalucaion;

        public delegate void delegateTiempoSituacion(float argTiempoSituacionActual);

        public delegate void delegateCantidadIntentos(byte argCantidadIntentosActuales);

        private delegateTiempoSituacion dltTiempoSituacion = delegate (float argTiempoSituacionActual)
        { };

        private delegateCantidadIntentos dltCantidadIntentos = delegate (byte argCantidadIntentosActuales)
        { };
 		#endregion

 		#region accesors

        public byte _cantidadIntentos
        {
            get
            {
                return cantidadIntentos;
            }
        }

        public float _tiempoSituacion
        {
            get
            {
                return tiempoSituacion;
            }
        }

        public delegateTiempoSituacion _listenerTiempoSituacion
        {
            get
            {
                return dltTiempoSituacion;                
            }
            set
            {
                dltTiempoSituacion = value;
            }
        }

        public delegateCantidadIntentos _listenerCantidadIntentos
        {
            get
            {
                return dltCantidadIntentos;
            }
            set
            {
                dltCantidadIntentos = value;
            }
        }        
        #endregion

        #region monoBehaviour

		// Update is called once per frame
 		void Update ()
		{
            ContarTiempo();
		}
 		#endregion

 		#region private methods

        private void ContarTiempo()
        {
            if (contarTiempoSituacion)
            {
                tiempoSituacion += Time.deltaTime;         
                dltTiempoSituacion(tiempoSituacion);                
            }
        }
 		#endregion

 		#region public methods

        public void SetContarTiempo(bool argContarTiempo)
        {
            contarTiempoSituacion = argContarTiempo;
            tiempoSituacion = 0f;
            dltTiempoSituacion(tiempoSituacion);
        }

        public void AddIntentos()
        {
            cantidadIntentos++;
             var aux= clsEvalucaion._refCalificacionSituacion;
            aux._cantidadIntentos=cantidadIntentos;
            dltCantidadIntentos(cantidadIntentos);
        }

        public void IniciarNuevaSesionSituacion()
        {
            SetContarTiempo(true);
            cantidadIntentos = 0;
            dltCantidadIntentos(cantidadIntentos);
        }
 		#endregion
	}
}