﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSEvaluacion
{
    [CreateAssetMenu(menuName = "Evaluacion/Preguntas Situacion")]
    /// <summary>
    /// Clase que contiene el sprite del enunciado, texto del enunciado, preguntas con respecto al enunciado, 
    /// respuestas para cada pregunta y respuestas falsas, esto para una evaluacion tipo PISA
    /// </summary>
    public class PreguntasSituacion : ScriptableObject
	{
        #region members
        /// <summary>
        /// Imagen del enunciado de la evaluacion
        /// </summary>
        [SerializeField]
        private Sprite imagenEnunciado;
        /// <summary>
        /// Texto del enunciado de la evaluacion 
        /// </summary>
        [SerializeField]
        private string textoEnunciado;
        /// <summary>
        /// Preguntas con respecto al enunciado, cada indice representa una pregunta diferente
        /// </summary>
        [SerializeField]
        private string[] preguntas;
        /// <summary>
        /// Respuestas con respecto a cada pregunta, cada indice corresponde directamente al array preguntas
        /// </summary>
        [SerializeField]
        private string[] respuestas;
        /// <summary>
        /// Respuestas imagen con respecto a cada pregunta, cada indice corresponde directamente al array preguntas
        /// </summary>
        [SerializeField]
        private Sprite[] respuestasImagen;
        /// <summary>
        /// Array de la clase que dentro tiene otro array que contiene respuestas falsas, cada indice de este array corresponde directamente al array preguntas
        /// </summary>
        [SerializeField]
        private ContendorRespuestasFalsas[] contenedorRespuestasFalsas;
        #endregion

        #region accesors

        public Sprite _imagenEnunciado
        {
            get
            {
                return imagenEnunciado;
            }
        }

        public string _textoEnunciado
        {
            get
            {
                return textoEnunciado;
            }
        }

        public string[] _preguntas
        {
            get                
            {
                return preguntas;
            }
        }

        public string[] _respuestas
        {
            get
            {
                return respuestas;
            }
        }

        public Sprite[] _respuestasImagen
        {
            get
            {
                return respuestasImagen;
            }
        }

        public ContendorRespuestasFalsas[] _contenedorRespuestasFalsas
        {
            get
            {
               return contenedorRespuestasFalsas;
            }
        }
        #endregion
    }

    [Serializable]
    public class ContendorRespuestasFalsas
    {
        #region members

        [SerializeField]
        private string[] respuestasFalsas;

        [SerializeField]
        private Sprite[] respuestasFalsasImagen;
        #endregion

        #region accesors

        public string[] _respuestasFalsas
        {
            get
            {
                return respuestasFalsas;
            }
        }

        public Sprite[] _respuestasFalsasImagen
        {
            get
            {
                return respuestasFalsasImagen;
            }
        }
        #endregion
    }
}