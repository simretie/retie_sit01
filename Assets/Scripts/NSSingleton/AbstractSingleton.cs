﻿using UnityEngine;

namespace NSSingleton
{
    public abstract class AbstractSingleton<T> : MonoBehaviour where T : AbstractSingleton<T>
    {
        protected static T instance;

        public static T _instance
        {
            get
            {
                if (instance == null)
                {
                    var tmpObjectsOfType = Resources.FindObjectsOfTypeAll<T>();

                    if (tmpObjectsOfType.Length > 0)
                        instance = tmpObjectsOfType[0];
                    else
                        instance = (new GameObject(typeof(T).ToString())).AddComponent<T>();
                }

                return instance;
            }
        }

        public static void DestroySingleton()
        {
            Destroy(instance.gameObject);
        }

        public void CreateInstance()
        {
            DontDestroyOnLoad(_instance.gameObject);
        }
    }
}
