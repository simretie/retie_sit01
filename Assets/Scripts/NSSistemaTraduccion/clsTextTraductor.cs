﻿using NSTraduccionIdiomas;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using TexDrawLib;

/// <summary>
/// Clase que se adjunta a cualquier text de la interfaz para que este se pueda traducir
/// </summary>
public class clsTextTraductor : MonoBehaviour
{
    #region monobehaviour

    private void Awake()
    {
        DiccionarioIdiomas._instance.DLTraducirForNameObj += TraducirGameObjetText;
    }

    // Use this for initialization
    private void Start()
    {
        TraducirGameObjetText();
    }
    #endregion

    #region private methods

    /// <summary>
    /// identifica el tipo de texto y lo traduse
    /// </summary>
    private void TraducirGameObjetText()
    {
        var tmpText = GetComponent<Text>();

        if (tmpText)
        {
            var traduccion = DiccionarioIdiomas._instance.Traducir(name, tmpText.text);
            tmpText.text = traduccion;
            return;
        }

        var tmpTextMeshProUGUI = GetComponent<TextMeshProUGUI>();

        if (tmpTextMeshProUGUI)
        {
            var traduccion = DiccionarioIdiomas._instance.Traducir(name, tmpTextMeshProUGUI.text);
            var textoConFormato = new StringBuilder(traduccion);
            tmpTextMeshProUGUI.SetText(textoConFormato);
            return;
        }

        var tmpTextParaTraducir = GetComponent<TextParaTraducir>();

        if (tmpTextParaTraducir)
        {
            var traduccion = DiccionarioIdiomas._instance.Traducir(name, tmpTextParaTraducir.text);
            tmpTextParaTraducir.text = traduccion;
            return;
        }

        var tmpTextTraducirDraw = GetComponent<TEXDraw>();

        if (tmpTextParaTraducir)
        {
            var traduccion = DiccionarioIdiomas._instance.Traducir(name, tmpTextParaTraducir.text);
            tmpTextTraducirDraw.text = traduccion;
            return;
        }


    }
    #endregion
}