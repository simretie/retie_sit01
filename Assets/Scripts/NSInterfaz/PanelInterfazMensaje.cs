﻿using NSAvancedUI;
using TMPro;
using UnityEngine;

namespace NSInterfaz
{
    public class PanelInterfazMensaje : AbstractPanelUIAnimation
    {
        #region members

        [SerializeField]
        private TextMeshProUGUI textMensaje;
        #endregion

        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        #endregion

        #region private methods

        #endregion

        #region public methods

        public void MtdActivarMensaje(string Mensaje)
        {
            textMensaje.text = Mensaje;
            Mostrar(true);
        }
        #endregion

        #region courutines

        #endregion
    }
}