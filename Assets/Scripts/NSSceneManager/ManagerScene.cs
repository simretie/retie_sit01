﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ManagerScene : MonoBehaviour {

    string[] actualScene = new string[4];
    private int Actual = 0;
    practicas activa;
    // [SerializeField]
    //private GameObject canvasMensajes;

    [SerializeField]
    private RectTransform canvasMensaje;

    [SerializeField]
    private GameObject CanvasMenuInicio;

    [SerializeField]
    private GameObject camaraScenaMenu;

    private enum practicas {
        practica1,
        practica2,
        practica3,
        practica4
    }

    // Use this for initialization
    void Start() {

        actualScene[0] = "practica1";
        actualScene[1] = "practica2";
        actualScene[2] = "practica3";
        actualScene[3] = "practica4";
       btnCargarPrectica(0);
    }

    // Update is called once per frame
    void Update() {

    }

    public void btnCargarPrectica(int scena)
    {
        //activa = practicas.practica1;
        SceneManager.LoadScene(actualScene[scena], LoadSceneMode.Additive);
        Actual = scena;
        //CanvasMenuInicio.SetActive(false);
    }

    /// <summary>
    /// descarga la scena de la practica activa
    /// </summary>
	public void descargarpractica()
    {
        CanvasMenuInicio.SetActive(true);
        camaraScenaMenu.SetActive(true);
        SceneManager.UnloadSceneAsync(actualScene[Actual]);

    }

    public void offCanvasMenuInicio()
    {
        if (CanvasMenuInicio.activeSelf)
        {
            //CanvasMenuInicio.SetActive(false);
            camaraScenaMenu.SetActive(false);
        }
    }


    public void recargarCanvasMesajes(){
        StartCoroutine(racargarCanvas());
    }

    IEnumerator racargarCanvas()
    {
        canvasMensaje.gameObject.SetActive(false);
        //Debug.Log("desactive el canvas");
        yield return new WaitForSeconds(0.5f);
        //Debug.Log("activar el canvas");
        canvasMensaje.gameObject.SetActive(true);
        //LayoutRebuilder.ForceRebuildLayoutImmediate(canvasMensaje);

    }
}
