﻿using UnityEngine;
using UnityEngine.UI;
public class clsInformacion : MonoBehaviour
{

    public GameObject[] InformacionSituacion;
    public GameObject[] InformacionProcedimiento;
    public GameObject[] InformacionEcuaciones;

    public Toggle situacion;
    public Toggle procedimento;
    public Toggle ecucion;

    // Use this for initialization
    private void Start()
    {
        MtdActivarSituacion();
    }

    public void MtdActivarSituacion()
    {
        DesactivarInfo();

        foreach (GameObject iterador in InformacionSituacion)
        {
            iterador.SetActive(true);
        }
    }

    public void MtdActivarProcedimeinto()
    {
        DesactivarInfo();

        foreach (GameObject iterador in InformacionProcedimiento)
        {
            iterador.SetActive(true);
        }
    }

    public void MtdActivarEcuaciones()
    {
        DesactivarInfo();

        foreach (GameObject iterador in InformacionEcuaciones)
        {
            iterador.SetActive(true);
        }
    }

    public void DesactivarInfo()
    {
        foreach (GameObject iterador in InformacionEcuaciones)
        {
            iterador.SetActive(false);
        }

        foreach (GameObject iterador in InformacionProcedimiento)
        {
            iterador.SetActive(false);
        }

        foreach (GameObject iterador in InformacionSituacion)
        {
            iterador.SetActive(false);
        }

    }

    public void mtdReiniciar()
    {
        situacion.isOn = true;
        procedimento.isOn = false;
        ecucion.isOn = false;
        MtdActivarSituacion();
    }
}
