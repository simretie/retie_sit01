﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadAspectRatio : MonoBehaviour {

	// Use this for initialization
	public bool change;
	public float value = 1.65f;
	public float camPos = -1366.1f;
	public float camFov = 70f;


	void Start () {

		if ((Screen.width * 1.0f / Screen.height * 1.0f) < value) {
			GetComponent<Camera> ().fieldOfView = camFov;
			transform.position = new Vector3 (transform.position.x, transform.position.y, camPos);
		}
	}
	
	 
}
