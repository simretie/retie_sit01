﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class FindSelectorAssignName : MonoBehaviour {

	// Use this for initialization
	public bool Buscar = false;
	public bool EditarObjetos = false;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if(Buscar){

			string totalText = "";
			Buscar = false;

			SelectorRetie3[] selectores = new SelectorRetie3[0];
			selectores = Resources.FindObjectsOfTypeAll<SelectorRetie3> ();

			int cantidad = 0;

			foreach(SelectorRetie3 selector in selectores){

				for(var i =0; i< selector.transform.childCount; i++){

					totalText += "*";
					totalText += "Texto" + selector.name + i.ToString () + selector.transform.GetChild (i).gameObject.transform.Find ("Nombre").GetComponent<Text> ().text.Replace (" ","").Replace("\r","").Replace("\n","");
					totalText += "=";
					totalText += selector.transform.GetChild (i).gameObject.transform.Find ("Nombre").GetComponent<Text> ().text; 

					Debug.Log ("Texto"+selector.name+i.ToString()+ selector.transform.GetChild(i).gameObject.transform.Find("Nombre").GetComponent<Text>().text.Replace (" ","").Replace("\r","").Replace("\n",""));
					Debug.Log (selector.transform.GetChild(i).gameObject.transform.Find("Nombre").GetComponent<Text>().text);

					if (EditarObjetos) {
						GameObject tempObj = selector.transform.GetChild (i).gameObject.transform.Find ("Nombre").gameObject;

						tempObj.name = "Texto" + selector.name + i.ToString () + selector.transform.GetChild (i).gameObject.transform.Find ("Nombre").GetComponent<Text> ().text.Replace (" ", "").Replace ("\r", "").Replace ("\n", "");
						tempObj.gameObject.AddComponent<clsTextTraductor> ();
					}
					cantidad++;


				}

			}
			Debug.Log (totalText);
		}

	}
}
